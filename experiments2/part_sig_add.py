from nmigen.cli import rtlil
from test_partsig import TestAddMod2, TestLS
import subprocess
import os
from nmigen import Signal

def test():
    width = 16
    pmask = Signal(3)  # divide into 4-bits
    #module = TestAddMod2(width, pmask)
    module = TestLS(width, pmask)
    sim = create_ilang(module, [pmask] + module.ports(),
                               "part_sig_add")
                            
def create_ilang(dut, ports, test_name):
    vl = rtlil.convert(dut, name=test_name, ports=ports)
    with open("%s.il" % test_name, "w") as f:
        f.write(vl)

if __name__ == "__main__":
    test()
