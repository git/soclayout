# generate add.il ilang file with: python3 add.py
#

from nmigen import Elaboratable, Signal, Module
from nmigen.cli import rtlil

# to get c4m-jtag
# clone with $ git clone gitolite3@git.libre-soc.org:c4m-jtag.git
#            $ git clone gitolite3@git.libre-soc.org:nmigen-soc.git
# for each:  $ python3 setup.py develop --user

from c4m.nmigen.jtag.tap import TAP, IOType


class ADD(Elaboratable):
    def __init__(self, width):
        self.a      = Signal(width)
        self.b      = Signal(width)
        self.f      = Signal(width)

        # set up JTAG
        self.jtag = TAP(ir_width=4)
        self.jtag.bus.tck.name = 'tck'
        self.jtag.bus.tms.name = 'tms'
        self.jtag.bus.tdo.name = 'tdo'
        self.jtag.bus.tdi.name = 'tdi'

        # have to create at least one shift register
        self.sr = self.jtag.add_shiftreg(ircode=4, length=3)

        # sigh and one iotype
        self.ios = self.jtag.add_io(name="test", iotype=IOType.In)

    def elaborate(self, platform):
        m = Module()

        m.submodules.jtag = jtag = self.jtag
        m.d.comb += self.sr.i.eq(self.sr.o) # loopback test

        # do a simple "add"
        m.d.sync += self.f.eq(self.a + self.b)

        return m


def create_ilang(dut, ports, test_name):
    vl = rtlil.convert(dut, name=test_name, ports=ports)
    with open("%s.il" % test_name, "w") as f:
        f.write(vl)

if __name__ == "__main__":
    alu = ADD(width=4)
    create_ilang(alu, [alu.a, alu.b, alu.f,
                       alu.jtag.bus.tck,
                        alu.jtag.bus.tms,
                        alu.jtag.bus.tdo,
                        alu.jtag.bus.tdi], "add")
