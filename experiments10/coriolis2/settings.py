# -*- Mode:Python -*-

from __future__ import print_function
import os
import Cfg
import CRL
import Viewer
#import node180.scn6m_deep_09
import symbolic.cmos45
from   helpers       import overlay, l, u, n

if os.environ.has_key('CELLS_TOP'):
    cellsTop = os.environ['CELLS_TOP']
else:
    cellsTop = '../../../alliance-check-toolkit/cells'
with overlay.CfgCache(priority=Cfg.Parameter.Priority.UserFile) as cfg:
    cfg.misc.catchCore = False
    cfg.misc.info = False
    cfg.misc.paranoid = False
    cfg.misc.bug = False
    cfg.misc.logMode = True
    cfg.misc.verboseLevel1 = True
    cfg.misc.verboseLevel2 = True
    cfg.etesian.graphics = 3
    cfg.etesian.spaceMargin = 0.05
    cfg.etesian.aspectRatio = 1.0
    cfg.anabatic.edgeLenght = 24
    cfg.anabatic.edgeWidth = 8
    cfg.anabatic.topRoutingLayer = 'METAL5'
    cfg.katana.eventsLimit = 4000000
    cfg.etesian.effort = 2
    cfg.etesian.uniformDensity = True
    cfg.katana.hTracksReservedLocal = 7
    cfg.katana.vTracksReservedLocal = 6
    Viewer.Graphics.setStyle( 'Alliance.Classic [black]' )
    af  = CRL.AllianceFramework.get()
    env = af.getEnvironment()
    env.setCLOCK( '^clk|^ck|^tck' )
    env.addSYSTEM_LIBRARY( library=cellsTop+'/niolib', mode=CRL.Environment.Prepend )
    env.addSYSTEM_LIBRARY( library=cellsTop+'/nsxlib', mode=CRL.Environment.Prepend )
print( '  o  Successfully run "<>/coriolis2/settings.py".' )
print( '     - CELLS_TOP = "{}"'.format(cellsTop) )
