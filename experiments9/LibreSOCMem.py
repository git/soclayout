
import CRL
import Hurricane
import Viewer
import Cfg
from   Hurricane import Technology, DataBase, DbU, Library,     \
                        Layer, BasicLayer, Cell, Net, Vertical, \
                        Rectilinear, Box, Point, Instance,      \
                        Transformation, NetExternalComponents,  \
                        Horizontal, Pad
from common.colors      import toRGB
from common.patterns    import toHexa
from helpers            import u, l
from helpers.technology import setEnclosures
from helpers.overlay    import CfgCache, UpdateSession


__all__ = ["setup"]


def _load():
    print( '  o  LibreSOCMem.py: create dummy abstract of "spblock_512w64b8w".' ) 

    af          = CRL.AllianceFramework.get()
    rg          = af.getRoutingGauge()
    cg          = af.getCellGauge()
    db          = DataBase.getDB()
    tech        = db.getTechnology()
    rootlib     = db.getRootLibrary()
    sliceHeight = cg.getSliceHeight()
    METAL2      = tech.getLayer( 'METAL2' )
    METAL3      = tech.getLayer( 'METAL3' )
    BLOCKAGE1   = tech.getLayer( 'BLOCKAGE1' )
    BLOCKAGE2   = tech.getLayer( 'BLOCKAGE2' )
    BLOCKAGE3   = tech.getLayer( 'BLOCKAGE3' )

    with UpdateSession():
        lib  = Library.create( rootlib, 'LibreSOCMem' )
        cell = Cell.create( lib, 'spblock_512w64b8w' )

        cell.setAbutmentBox( Box( 0, 0, (55+2)*sliceHeight, (45+2)*sliceHeight ) )
        nets = { 'clk'        : Net.create( cell, 'clk' )
               , 'vdd'        : Net.create( cell, 'vdd' )
               , 'vss'        : Net.create( cell, 'vss' )
               , 'blockageNet': Net.create( cell, 'blockageNet' )
               }
        nets['clk'].setType( Net.Type.CLOCK )
        nets['clk'].setExternal( True )
        nets['clk'].setDirection( Net.Direction.IN )
        nets['vdd'].setType( Net.Type.POWER )
        nets['vdd'].setExternal( True )
        nets['vdd'].setDirection( Net.Direction.IN )
        nets['vss'].setType( Net.Type.GROUND )
        nets['vss'].setExternal( True )
        nets['vss'].setDirection( Net.Direction.IN )
        blockageArea = Box( cell.getAbutmentBox() )
        blockageArea.inflate( -sliceHeight )
        for layer in (BLOCKAGE1, BLOCKAGE2, BLOCKAGE3):
            Pad.create( nets['blockageNet'], layer, blockageArea )
        for name, width in ( ('a', 9), ('d', 64), ('q', 64), ('we', 8) ):
            for bit in range(width):
                bitName = '{}({})'.format( name, bit )
                nets[ bitName ] = Net.create( cell, bitName )
                nets[ bitName ].setExternal( True )
                direction = Net.Direction.IN
                if name == 'q':
                    direction = Net.Direction.OUT
                nets[ bitName ].setDirection( direction )
        METAL2pitch   = rg.getLayerGauge( METAL2 ).getPitch()
        METAL3pitch   = rg.getLayerGauge( METAL3 ).getPitch()
        METAL2width   = rg.getLayerGauge( METAL2 ).getWireWidth()
        METAL3width   = rg.getLayerGauge( METAL3 ).getWireWidth()
        southSideStep = 16
        yref = cell.getAbutmentBox().getYMin() + sliceHeight - METAL2pitch
        for bitpair in range(32):
            xref = cell.getAbutmentBox().getXMax() \
                   - sliceHeight - ( (bitpair+1)*southSideStep + 2 )*METAL3pitch 

            pinSpecs = [ ('q', (63-bitpair*2)  ,  0)
                       , ('d', (63-bitpair*2)  , 10)
                       , ('d', (63-bitpair*2)-1, 12)
                       , ('q', (63-bitpair*2)-1, 14)
                       ]
            if bitpair%4 == 3:
                pinSpecs.append( ('we', 7-bitpair//4, 8) )
            for name, bit, deltaPitch in pinSpecs:
                net     = nets[ '{}({})'.format(name,bit) ]
                xaxis   = xref - METAL3pitch*deltaPitch
                segment = Vertical.create( net
                                         , METAL3
                                         , xaxis
                                         , METAL3width
                                         , yref
                                         , yref+8*METAL2pitch )
                segment = Vertical.create( net
                                         , METAL3
                                         , xaxis
                                         , METAL3width
                                         , yref
                                         , yref+METAL2pitch )
                NetExternalComponents.setExternal( segment )
        segment = Vertical.create( nets['clk']
                                 , METAL3
                                 , cell.getAbutmentBox().getXMin()
                                   + sliceHeight + 5*METAL3pitch
                                 , METAL3width
                                 , yref
                                 , yref+8*METAL2pitch )
        segment = Vertical.create( nets['clk']
                                 , METAL3
                                 , cell.getAbutmentBox().getXMin()
                                   + sliceHeight + 5*METAL3pitch
                                 , METAL3width
                                 , yref
                                 , yref+METAL2pitch )
        NetExternalComponents.setExternal( segment )

        xref = cell.getAbutmentBox().getXMin() + sliceHeight - METAL3pitch
        yref = cell.getAbutmentBox().getYMax() - sliceHeight
        for bit in range(9):
            net   = nets[ '{}({})'.format('a',8-bit) ]
            yaxis = yref - 2*sliceHeight*bit - METAL3pitch*5
            if bit > 4:
                yaxis -= 20*sliceHeight
            segment = Horizontal.create( net
                                       , METAL2
                                       , yaxis
                                       , METAL2width
                                       , xref
                                       , xref+8*METAL3pitch )
            segment = Horizontal.create( net
                                       , METAL2
                                       , yaxis
                                       , METAL2width
                                       , xref
                                       , xref+METAL3pitch )
            NetExternalComponents.setExternal( segment )

        xmin = cell.getAbutmentBox().getXMin() + 2*sliceHeight
        xmax = cell.getAbutmentBox().getXMax() - 2*sliceHeight
        yref = cell.getAbutmentBox().getYMin() + 2*sliceHeight
        for i in range(44):
            net = nets['vdd']
            if i % 2:
                net = nets['vss']
            segment = Horizontal.create( net
                                       , METAL3
                                       , yref + sliceHeight*i
                                       , l(24.0)
                                       , xmin
                                       , xmax )
            NetExternalComponents.setExternal( segment )
            
    af.wrapLibrary( lib, 0 )

    return lib

def setup():
    lib = _load()
    return lib
