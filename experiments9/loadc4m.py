#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import os.path
import Cfg
import Hurricane
from   Hurricane  import DbU, DataBase, UpdateSession, Library, Cell
import Viewer     
import CRL        
from   helpers.io                import ErrorMessage, WarningMessage
from   helpers.overlay           import UpdateSession
from   plugins.alpha.macro.macro import Macro
from CRL import Gds

from   helpers              import l, u, n
from   Hurricane            import DbU
from   Hurricane            import DataBase
from   Hurricane            import Technology

import common

def create_gds():

    tech = DataBase.getDB().getTechnology()
    if tech:
      print WarningMessage( 'cmos.technology: Technology already exists, "%s"' % tech.getName() )
    else:
      tech = Technology.create( DataBase.getDB(), 'c4mgds' )

    # Table guessed from the GDSII layouts of the cells.
    #    FreePDK45/osu_soc/lib/source/gds/*.gds
    #
    # Format of an entry in the table:
    #     (Symbolic_Name, CIF_Name, GDSII_Number)
    gdsLayersTable = \
        ( ("pWell"   , "CWN"    ,  2, 0)
        , ("nWell"   , "CWP"    ,  1, 0)
        , ("active"  , "CAA"    ,  5, 0)
        , ("pImplant", "CSP"    ,  8, 0)
        , ("nImplant", "CSN"    ,  7, 0)
        , ("poly"    , "CPG"    , 15, 0)
        , ("cut0"    , "CCC"    , 16, 0)
        , ("metal1"  , "CM1"    , 18, 0) # guessing
        , ("cut1"    , "CV1"    , 22, 0)
        , ("metal2"  , "CM2"    , 23, 0)
        , ("cut2"    , "CV2"    , 24, 0)
        , ("metal3"  , "CM3"    , 25, 0)
        , ("cut3"    , "CV3"    , 26, 0)
        , ("metal4"  , "CM4"    , 27, 0)
        , ("cut4"    , "CV4"    , 28, 0)
        , ("metal5"  , "CM5"    , 29, 0)
        , ("cut5"    , "CV5"    , 30, 0)
        , ("metal6"  , "CM6"    , 31, 0)
        , ("cut6"    , "CV6"    , 32, 0)
        , ("metal7"  , "CM7"    , 33, 0)
        , ("cut7"    , "CV7"    , 34, 0)
        , ("metal8"  , "CM8"    , 35, 0)
        , ("cut8"    , "CV8"    , 36, 0)
        , ("metal9"  , "CM9"    , 37, 0)
        , ("cut9"    , "CV9"    , 38, 0)
        , ("metal10" , "CM10"   , 150, 0) # errr block? somewhere?
        )


    common.loadGdsLayers( gdsLayersTable )

def scriptMain ( **kw ):
    create_gds()
    editor = None
    if kw.has_key('editor') and kw['editor']:
      editor = kw['editor']
    with UpdateSession():
        rootLibrary = DataBase.getDB().getRootLibrary()
        gdsLibrary = rootLibrary.getLibrary('gds')
        if not gdsLibrary:
            gdsLibrary = Library.create(rootLibrary, "GDS")
        Gds.load( gdsLibrary, "C4MLogo.gds") 
        c4m = gdsLibrary.getCell( 'C4MLogo' )
        print ("c4m", c4m)
        if editor: editor.setCell( c4m )
    return c4m

if __name__ == '__main__':
    #from coriolis2 import settings
    scriptMain()
