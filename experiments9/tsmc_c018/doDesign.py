
import os
import re
import json
import sys
import traceback
import collections
import CRL
import helpers
from   helpers         import trace, l, u, n
from   helpers.io      import ErrorMessage, WarningMessage
from   helpers.overlay import UpdateSession
import plugins
from   Hurricane  import Breakpoint, DataBase, DbU, Transformation, Point, Box, \
                         Cell, Instance
from   plugins.alpha.block.matrix         import RegisterMatrix
from   plugins.alpha.macro.macro          import Macro
from   plugins.alpha.block.iospecs        import IoSpecs
from   plugins.alpha.block.block          import Block
from   plugins.alpha.block.configuration  import IoPin, GaugeConf
from   plugins.alpha.block.spares         import Spares
from   plugins.alpha.core2chip.libresocio import CoreToChip
from   plugins.alpha.chip.configuration   import ChipConf
from   plugins.alpha.chip.chip            import Chip
#from   plugins.alpha.utils                import rgetInstanceMatching


af = CRL.AllianceFramework.get()
powerCount = 0
placeHolderCount = 0


def onGrid ( v ):
    twoGrid = DbU.fromGrid( 2 )
    modulo = v % twoGrid
    if modulo:
        v += twoGrid - modulo
    return v


def isiterable ( pyobj ):
    if isinstance(pyobj,collections.Iterable): return True
    return False


def doIoPowerCap ( flags ):
    global powerCount
    side = flags & IoPin.SIDE_MASK
    if flags & IoPin.A_BEGIN:
        ioPadPower = [ (side , None,    'power_{}'.format(powerCount),   'vdd' )
                     , (side , None,   'ground_{}'.format(powerCount),   'vss' )
                     , (side , None, 'ioground_{}'.format(powerCount),   'vss' )
                     , (side , None,  'iopower_{}'.format(powerCount), 'iovdd' )
                     ]
    else:
        ioPadPower = [ (side , None,  'iopower_{}'.format(powerCount), 'iovdd' )
                     , (side , None, 'ioground_{}'.format(powerCount),   'vss' )
                     , (side , None,   'ground_{}'.format(powerCount),   'vss' )
                     , (side , None,    'power_{}'.format(powerCount),   'vdd' )
                     ]
    powerCount += 1
    return ioPadPower


def doIoPinVector ( ioSpec, bits ):
    v = []
    if not isiterable(bits): bits = range(bits)
    if not bits:
        raise ErrorMessage( 1, [ 'doIoPinVector(): Argument "bits" is neither a width nor an iterable.'
                               , '(bits={})'.format(bits)
                               ] )
    if len(ioSpec) == 5:
        for bit in bits:
            v.append(( ioSpec[0]
                     , ioSpec[1]
                     , ioSpec[2].format(bit)
                     , ioSpec[3].format(bit)
                     , ioSpec[4].format(bit) ))
    elif len(ioSpec) == 6:
        for bit in bits:
            v.append(( ioSpec[0]
                     , ioSpec[1]
                     , ioSpec[2].format(bit)
                     , ioSpec[3].format(bit)
                     , ioSpec[4].format(bit)
                     , ioSpec[5].format(bit) ))
    elif len(ioSpec) == 7:
        for bit in bits:
            v.append(( ioSpec[0]
                     , ioSpec[1]
                     , ioSpec[2].format(bit)
                     , ioSpec[3].format(bit)
                     , ioSpec[4].format(bit)
                     , ioSpec[5].format(bit)
                     , ioSpec[6].format(bit) ))
    else:
        raise ErrorMessage( 1, [ 'doIoPinVector(): Argument "ioSpec" must have between 5 and 7 fields ({})'.format(len(ioSpec))
                               , '(ioSpec={})'.format(ioSpec)
                               ] )
    return v


def rgetInstance ( cell, path ):
    """
    Get the instance designated by path (recursively). The path argument can be
    either a string of instance names separated by dots or directly a list of
    instances names.
    """
    if isinstance(path,str):
        path = path.split( '.' )
    elif not isinstance(path,list):
        raise ErrorMessage( 1, 'rgetInstance(): "path" argument is neither a string or a list ({})"' \
                               .format(path) )
    instance = cell.getInstance( path[0] )
    if instance is None:
        raise ErrorMessage( 1, 'rgetInstance(): no instance "{}" in cell "{}"' \
                               .format(path[0],cell.getName()) )
    if len(path) == 1:
        return instance
    return rgetInstance( instance.getMasterCell(), path[1:] )


def rsetAbutmentBox ( cell, ab ):
    for occurrence in cell.getNonTerminalNetlistInstanceOccurrences():
        masterCell = occurrence.getEntity().getMasterCell()
        masterCell.setAbutmentBox( ab )


def scriptMain (**kw):
    """The mandatory function to be called by Coriolis CGT/Unicorn."""
    global af
   #helpers.setTraceLevel( 550 )
   #Breakpoint.setStopLevel( 100 )
    rvalue     = True
    coreSizeX  = u(51*90.0)
    coreSizeY  = u(56*90.0)
    chipBorder = u(2*214.0 + 8*13.0)
    ioSpecs    = IoSpecs()

    # this should work fine, tested on nsxlib
    cwd = os.path.split(os.path.abspath(__file__))[0]
    pinmuxFile = '%s/non_generated/litex_pinpads.json' % cwd
    # actual contents auto-generated and listed at:
    # http://libre-soc.org/180nm_Oct2020/ls180/
    ioSpecs.loadFromPinmux( pinmuxFile )

    # XXX ioPadsSpec created but not used.  saves time, saves errors. see
    # wiki page for contents: http://libre-soc.org/180nm_Oct2020/ls180/
    # if *not* using the auto-generated ioSpecs, ioPadsSpec should, really,
    # be made exactly the same. which is more work.

    # I/O pads, East side.
    ioPadsSpec  = []
    ioPadsSpec += doIoPowerCap( IoPin.EAST|IoPin.A_BEGIN )
    ioPadsSpec += [ (IoPin.EAST, None, 'sdram_cas_n'   , 'sdram_cas_n'   , 'sdram_cas_n' )
                  , (IoPin.EAST, None, 'sdram_we_n'    , 'sdram_we_n'    , 'sdram_we_n' )
                  , (IoPin.EAST, None, 'sdram_cs_n'    , 'sdram_cs_n'    , 'sdram_cs_n' )
                  ]
    ioPadsSpec += doIoPinVector( (IoPin.EAST, None, 'sdram_a_{}' , 'sdram_a({})' , 'sdram_a({})'), 13 )
    ioPadsSpec += doIoPinVector( (IoPin.EAST, None, 'sdram_ba_{}', 'sdram_ba({})', 'sdram_ba({})'), 2 )
    ioPadsSpec += doIoPinVector( (IoPin.EAST, None, 'sdram_dm_{}', 'sdram_dm({})', 'sdram_dm({})'), 2 )
    ioPadsSpec += doIoPinVector( (IoPin.EAST, None, 'nc_{}', 'nc({})', 'nc({})'), range(0,2) )
    ioPadsSpec += doIoPowerCap( IoPin.EAST|IoPin.A_END )
    ioPadsSpec += [ (IoPin.EAST             , None, 'sys_pll_testout_o', 'sys_pll_testout_o', 'sys_pll_testout_o' )
                  , (IoPin.EAST|IoPin.ANALOG, None, 'sys_pll_vco_o'    , 'sys_pll_vco_o'    , 'sys_pll_vco_o' )
                  ]

   # I/O pads, North side.
    ioPadsSpec += doIoPowerCap( IoPin.NORTH|IoPin.A_BEGIN )
    ioPadsSpec += [ (IoPin.NORTH, None, 'jtag_tms'     , 'jtag_tms'     , 'jtag_tms' )
                  , (IoPin.NORTH, None, 'jtag_tdi'     , 'jtag_tdi'     , 'jtag_tdi' )
                  , (IoPin.NORTH, None, 'jtag_tdo'     , 'jtag_tdo'     , 'jtag_tdo' )
                  , (IoPin.NORTH, None, 'jtag_tck'     , 'jtag_tck'     , 'jtag_tck' )
                  ]
    ioPadsSpec += doIoPinVector( (IoPin.NORTH, None, 'nc_{}', 'nc({})', 'nc({})'), range(2,19) )
    ioPadsSpec += doIoPinVector( (IoPin.NORTH, None, 'sdram_dq_{}', 'sdram_dq({})', 'sdram_dq_i({})', 'sdram_dq_oe({})', 'sdram_dq_o({})'), range(0,16) )
    ioPadsSpec += doIoPinVector( (IoPin.NORTH, None, 'sys_clksel_i{}', 'sys_clksel_i({})', 'sys_clksel_i({})'), 2 )
    ioPadsSpec += [ (IoPin.NORTH, None, 'sys_clk'      , 'sys_clk'      , 'sys_clk' ) ]
    ioPadsSpec += doIoPowerCap( IoPin.NORTH|IoPin.A_END )

   # I/O pads, West side.
    ioPadsSpec += doIoPowerCap( IoPin.WEST|IoPin.A_BEGIN )
    ioPadsSpec += doIoPinVector( (IoPin.WEST , None,   'nc_{}', '  nc({})',   'nc({})'), range(19,36) )
    ioPadsSpec += doIoPinVector( (IoPin.WEST , None, 'eint_{}',  'eint_{}',  'eint_{}'),  3 )
    ioPadsSpec += [ (IoPin.WEST , None, 'spimaster_clk' , 'spimaster_clk' , 'spimaster_clk' )
                  , (IoPin.WEST , None, 'spimaster_cs_n', 'spimaster_cs_n', 'spimaster_cs_n' )
                  , (IoPin.WEST , None, 'spimaster_mosi', 'spimaster_mosi', 'spimaster_mosi' )
                  , (IoPin.WEST , None, 'spimaster_miso', 'spimaster_miso', 'spimaster_miso' )
                  ]
    ioPadsSpec += doIoPowerCap( IoPin.WEST|IoPin.A_END )

   # I/O pads, South side.
    ioPadsSpec += doIoPowerCap( IoPin.SOUTH|IoPin.A_BEGIN )
    ioPadsSpec += doIoPinVector( (IoPin.SOUTH, None, 'gpio_{}', 'gpio({})', 'gpio_i({})', 'gpio_oe({})', 'gpio_o({})'), range(0,16) )
    ioPadsSpec += [ (IoPin.SOUTH, None, 'i2c_sda_i'     , 'i2c_sda_i'     , 'i2c_sda_i', 'i2c_sda_oe', 'i2c_sda_o' ) ]
    ioPadsSpec += [ (IoPin.SOUTH, None, 'i2c_scl'       , 'i2c_scl'       , 'i2c_scl' ) ]
    ioPadsSpec += [ (IoPin.SOUTH, None, 'uart_tx', 'uart_tx', 'uart_tx' )
                  , (IoPin.SOUTH, None, 'uart_rx', 'uart_rx', 'uart_rx' )
                  , (IoPin.SOUTH, None, 'sys_rst', 'sys_rst', 'sys_rst' )
                  ]
    ioPadsSpec += [ (IoPin.SOUTH, None, 'sdram_clock'   , 'sdram_clock'   , 'sdram_clock' )
                  , (IoPin.SOUTH, None, 'sdram_cke'     , 'sdram_cke'     , 'sdram_cke' )
                  , (IoPin.SOUTH, None, 'sdram_ras_n'   , 'sdram_ras_n'   , 'sdram_ras_n' )
                  ]
    ioPadsSpec += doIoPowerCap( IoPin.SOUTH|IoPin.A_END )

    try:
        cell, editor = plugins.kwParseMain( **kw )
        cell = af.getCell( 'ls180', CRL.Catalog.State.Logical )
        if cell is None:
            print( ErrorMessage( 2, 'doDesign.scriptMain(): Unable to load cell "{}".' \
                                    .format('ls180') ))
            sys.exit(1)
        if editor: editor.setCell( cell )
        # use auto-generated (but from non_generated) io pads specs
        # works fine with soclayout nsxlib, should work perfectly fine
        # here, too
        ls180Conf = ChipConf( cell, ioPads=ioSpecs.ioPadsSpec )
        #ls180Conf = ChipConf( cell, ioPads=ioPadsSpec )
        ls180Conf.cfg.etesian.bloat = 'Flexlib'
        ls180Conf.cfg.etesian.uniformDensity = True
        ls180Conf.cfg.etesian.aspectRatio = 1.0
        ls180Conf.cfg.etesian.spaceMargin = 0.05
        ls180Conf.cfg.anabatic.searchHalo = 3
        ls180Conf.cfg.anabatic.globalIterations = 20
        ls180Conf.cfg.anabatic.topRoutingLayer = 'METAL5'
        ls180Conf.cfg.katana.hTracksReservedLocal = 11
        ls180Conf.cfg.katana.vTracksReservedLocal = 8
        ls180Conf.cfg.katana.hTracksReservedMin = 8
        ls180Conf.cfg.katana.vTracksReservedMin = 6
        ls180Conf.cfg.katana.runRealignStage = True
        ls180Conf.cfg.katana.trackFill = 0
        ls180Conf.cfg.block.spareSide = u(7*13)
        ls180Conf.cfg.chip.supplyRailWidth = u(35)
        ls180Conf.cfg.chip.supplyRailPitch = u(90)
        ls180Conf.editor = editor
        ls180Conf.useSpares = True
        ls180Conf.useClockTree = True
        ls180Conf.useHFNS = True
        ls180Conf.bColumns = 2
        ls180Conf.bRows = 2
        ls180Conf.chipConf.name = 'chip'
        ls180Conf.chipConf.ioPadGauge = 'LibreSOCIO'
        ls180Conf.coreSize = (coreSizeX, coreSizeY)
        ls180Conf.chipSize = (coreSizeX + chipBorder + u(5.0), coreSizeY + chipBorder - u(0.04) )
        ls180Conf.chipLogos = [ 'C4MLogo_norm'
                              , 'libresoc_logo_norm'
                              , 'sorbonne_logo_norm'
                              , 'lip6_norm'
                              ]
        ls180Conf.useHTree( 'core.pll_clk', Spares.HEAVY_LEAF_LOAD )
        ls180Conf.useHTree( 'jtag_tck_from_pad' )

        tiPath = 'test_issuer.ti.'
        sramDatas \
            = [ ['test_issuer.ti.sram4k_0.spblock_512w64b8w', -2]
              , ['test_issuer.ti.sram4k_1.spblock_512w64b8w',  2]
              , ['test_issuer.ti.sram4k_2.spblock_512w64b8w',  2]
              , ['test_issuer.ti.sram4k_3.spblock_512w64b8w',  2]
              ]

        ls180ToChip = CoreToChip( ls180Conf )
        ls180ToChip.buildChip()
        chipBuilder = Chip( ls180Conf )
        chipBuilder.doChipFloorplan()

        with UpdateSession():
            sram        = DataBase.getDB().getCell( 'spblock_512w64b8w' )
            sramAb      = sram.getAbutmentBox()
            coreAb      = cell.getAbutmentBox()
            sliceHeight = chipBuilder.conf.sliceHeight
            sliceStep   = chipBuilder.conf.sliceStep
            originX     = coreAb.getXMin() + sramDatas[0][1]*chipBuilder.conf.sliceStep
            for i in range(len(sramDatas)):
                chipBuilder.placeMacro \
                    ( sramDatas[i][0]
                    , Transformation( originX
                                    , coreAb.getYMax() - sramAb.getHeight() - 2*sliceHeight
                                    , Transformation.Orientation.ID )
                    )
                if i+1 < len(sramDatas):
                    originX += sramAb.getWidth() + 2*sliceHeight + sramDatas[i+1][1]*sliceStep
            pllTransf = Transformation( coreAb.getXMax() # -u(234.0)
                                      , coreAb.getYMax() - u(208.0)
                                      , Transformation.Orientation.MX )
            print( 'pllTransf={}'.format(pllTransf) )
            chipBuilder.placeMacro( 'test_issuer.wrappll.pll' , pllTransf )
        sys.stderr.flush()
        sys.stdout.flush()
        Breakpoint.stop( 99, 'After core placement.' )

        rvalue = chipBuilder.doPnR()
        chipBuilder.save()
        CRL.Gds.save( ls180Conf.chip )
    except Exception as e:
        helpers.io.catch(e)
        rvalue = False
    sys.stdout.flush()
    sys.stderr.flush()
    return rvalue
