

================================
Quick Guidelines on Floorplaning
================================


Some hints about how to avoid common pitfall making the floorplan.

Terminology:

* ``Cell`` : Even if in Hurricane it contain both the netlist and
  the layout, in this context we will consider it as a pure netlist.

* ``Block`` : The Python class wrapper around a ``Cell`` to change it
  into a placed and routed layout used as black box by the above
  hierarchical levels. It is both the program that perform the P&R
  and a cache for upper levels.
    If it contains no sub-blocks, it is placed flat (virtual flatten).


Disclaimer
==========

At this moment, the Block tools can hardly be called floorplaner.
They provides very basic facility for manual floorplanning.


Layout Caching
==============

To create a block from a ``Cell``:

.. code-block:: python

   add = af.getCell( 'add', CRL.Catalog.State.Views )
   blockAdd = Block.create \
       ( add
       , ioPins=[ (IoPin.SOUTH|IoPin.A_BEGIN, 'a({})', l( 10.0), l(20.0), 16)
                , (IoPin.SOUTH|IoPin.A_BEGIN, 'b({})', l( 20.0), l(20.0), 16)
                , (IoPin.NORTH|IoPin.A_BEGIN, 'o({})', l(100.0), l(10.0), 16)
                ]
       )
   blockAdd.state.cfg.etesian.spaceMargin = 0.10
   blockAdd.state.fixedHeight = l(400)
   blockAdd.state.useSpares   = False
   blockAdd.state.editor      = editor
   rvalue = blockAdd.build()


If you load the ``Cell`` with the flag ``CRL.Catalog.State.Views``, the
parser will attemps to load both the netlist (mandatory) and the layout.
If the layout is present, then the block **will not be P&R again**,
the current layout is reused.

If you want to selectivelely rebuild one block, you can either remove
the relevant ``ap`` file or change the loading flag to:

.. code-block:: python

   add = af.getCell( 'add', CRL.Catalog.State.Logical )


Pin Placement
=============

Pin placement is critical both for the P&R of the block and the one
of the above level. So here is some hints.

* Do not pack pin as closely as possible. Try to leave at least one
  free pitch between them. Otherwise it may create huge contention
  point both inside the block and outside to access the block.

* If there is a *sliced* structure, that is one same treatment is
  done for each bit of the I/O busses, interleave the relevant
  busses pins. For examle, instead of having: ::

    a(0) a(1) a(2) a(3) b(0) b(1) b(2) b(3)

  Try: ::

    a(0) b(0) a(1) b(1) a(2) b(2) a(3) b(3)


Aspect Ratio (aka Form Factor)
==============================

It is important to consider that the routing capacity in one direction
is directly proportional to the length of the side of the abutment box
in the perpandicular direction. So, if you make a very *flat* box,
the number of horizontal tracks became limited, and may become not
routable.

This effect is related to the kind of graph of the netlist. If it
has a strong pipeline structure, a very elongated shape may be still
be possible.


The Halo Parameter
==================

By default, the global router will search to connect all pins of a Net
in the square area defined by the pins, inflated by the halo (the halo
is given in number of GCells, that is the height of a standard cell).
A problem arise if you have a net of only two pins (usually 60% of the
nets are like that), each across of a huge block. To avoid being stuck,
the halo must be increased by at least half the size of the biggest
block dimension. Note that, by drastically increasing the search area
it may slow down much the global router.

.. code-block:: python

   blockIssuer.state.cfg.katana.searchHalo = 10000
 
