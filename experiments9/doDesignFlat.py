
from   __future__ import print_function
import sys
import traceback
import CRL
import Cfg
import helpers
from   helpers.io      import ErrorMessage
from   helpers.io      import WarningMessage
from   helpers.overlay import CfgCache
from   helpers         import trace, l
import plugins
from   Hurricane  import DbU
from   Hurricane  import Pin
from   Hurricane  import Transformation
from   plugins.alpha.block.block         import Block
from   plugins.alpha.block.configuration import IoPin


af = CRL.AllianceFramework.get()


def coriolis_setup():
    with CfgCache(priority=Cfg.Parameter.Priority.UserFile) as cfg:
        cfg.misc.catchCore = False
        cfg.misc.info = False
        cfg.misc.paranoid = False
        cfg.misc.bug = False
        cfg.misc.logMode = True
        cfg.misc.verboseLevel1 = True
        cfg.misc.verboseLevel2 = True
        cfg.etesian.effort = 2
        cfg.etesian.spaceMargin = 0.10  # 10%
        cfg.etesian.aspectRatio = 1.0   # 100%
        cfg.etesian.uniformDensity = True
        cfg.anabatic.edgeLenght = 24
        cfg.anabatic.edgeWidth = 8
        cfg.anabatic.topRoutingLayer = 'METAL5'
        cfg.katana.searchHalo = 30
        cfg.katana.eventsLimit = 1000000
        cfg.katana.hTracksReservedLocal = 7
        cfg.katana.vTracksReservedLocal = 6

        env = af.getEnvironment()
        env.setCLOCK('^clk$|m_clock')
        env.setPOWER('vdd')
        env.setGROUND('vss')


def scriptMain ( **kw ):
    """The mandatory function that Coriolis CGT/Unicorn will look for."""
    global af

    IW = IoPin.WEST
    IE = IoPin.EAST
    IS = IoPin.SOUTH
    IN = IoPin.NORTH
    AB = IoPin.A_BEGIN
    AE = IoPin.A_END

    rvalue = True
    try:
        #helpers.setTraceLevel( 550 )
        cell, editor = plugins.kwParseMain( **kw )

        # Quick and simple placement. I/O busses are grouped together.
        # It may be useful to better choose the sides to give a hint to
        # the placer about how to "blocks" must be positionned inside the
        # chip.
        #
        # Note: Yes, blocks should be kept by the placer, in a fuzzy way.
        issuerp = [
            # Bit inputs.
            (IN     , 'clk'             , l( 7050.0) ),
            (IN | AB, 'rst'             , l( 7000.0) ),
            (IN | AB, 'core_bigendian_i', l( 7100.0) ),
            (IN | AB, 'dbus_ack'        , l( 7200.0) ),
            (IN | AB, 'dbus_err'        , l( 7300.0) ),
            (IN | AB, 'ibus_ack'        , l( 7400.0) ),
            (IN | AB, 'ibus_err'        , l( 7500.0) ),
            (IN | AB, 'ibus_we'         , l( 7600.0) ),
            (IN | AE, 'ibus_stb'        , l( 7700.0) ),
            (IN | AB, 'dmi_req_i'       , l( 7800.0) ),
            (IN | AB, 'dmi_we_i'        , l( 7900.0) ),
            (IN | AB, 'memerr_o'        , l( 8000.0) ),
            (IN | AB, 'pc_i_ok'         , l( 8100.0) ),
            # Bit output.                    
            (IN | AB, 'busy_o'          , l( 8200.0) ),
            (IN | AB, 'dbus_cyc'        , l( 8300.0) ),
            (IN | AB, 'dbus_stb'        , l( 8400.0) ),
            (IN | AB, 'dbus_we'         , l( 8500.0) ),
            (IN | AB, 'dmi_ack_o'       , l( 8600.0) ),
            (IN | AB, 'ibus_cyc'        , l( 8700.0) ),
            # Busses.
            (IW | AB, 'dbus_bte({})'    , l(  500.0), l( 50.0),  2),
            (IW | AB, 'dbus_cti({})'    , l(  600.0), l( 50.0),  3),
            (IW | AB, 'dbus_sel({})'    , l(  750.0), l( 50.0),  8),
            (IW | AB, 'dbus_adr({})'    , l( 1150.0), l( 50.0), 45),
            (IW | AB, 'dbus_dat_r({})'  , l( 3400.0), l( 50.0), 64),
            (IW | AB, 'dbus_dat_w({})'  , l( 6600.0), l( 50.0), 64),
            (IW | AB, 'ibus_bte({})'    , l( 9800.0), l( 50.0),  2),
            (IW | AB, 'ibus_cti({})'    , l( 9900.0), l( 50.0),  3),
            (IW | AB, 'ibus_sel({})'    , l(10050.0), l( 50.0),  8),
            (IW | AB, 'ibus_adr({})'    , l(10450.0), l( 50.0), 45),
            (IN | AB, 'ibus_dat_r({})'  , l(  500.0), l( 50.0), 64),
            (IN | AE, 'ibus_dat_w({})'  , l( 3700.0), l( 50.0), 64),
            (IS | AB, 'dmi_addr_i({})'  , l(  500.0), l( 50.0),  4),
            (IS | AB, 'dmi_din({})'     , l(  700.0), l( 50.0), 64),
            (IS | AB, 'dmi_dout({})'    , l( 3900.0), l( 50.0), 64),
            (IS | AB, 'pc_i({})'        , l( 7100.0), l( 50.0), 64),
            (IS | AB, 'pc_o({})'        , l(10300.0), l( 25.0), 64),
        ]
 
        issuer      = af.getCell( 'test_issuer' , CRL.Catalog.State.Logical )
        blockIssuer = Block.create( issuer , ioPins=issuerp )
        blockIssuer.state.cfg.etesian.uniformDensity      = True
        blockIssuer.state.cfg.etesian.aspectRatio         = 1.0
        blockIssuer.state.cfg.etesian.spaceMargin         = 0.15
        blockIssuer.state.cfg.katana.searchHalo           = 1
        blockIssuer.state.cfg.katana.vTracksReservedLocal = 7
        blockIssuer.state.cfg.katana.hTracksReservedLocal = 6
        blockIssuer.state.cfg.katana.hTracksReservedMin   = 2
        blockIssuer.state.cfg.katana.vTracksReservedMin   = 1
        blockIssuer.state.cfg.katana.bloatOverloadAdd     = 4
        blockIssuer.state.cfg.block.spareSide             = l(400)
       #blockIssuer.state.fixedHeight = l(15000)
       #blockIssuer.state.fixedWidth  = l(31550)
        blockIssuer.state.editor       = editor
        blockIssuer.state.useSpares    = True
        blockIssuer.state.useClockTree = True
        blockIssuer.state.bColumns     = 6
        blockIssuer.state.bRows        = 6
        rvalue = blockIssuer.build()
    except Exception, e:
        helpers.io.catch( e )
        rvalue = False

    sys.stdout.flush()
    sys.stderr.flush()
    return rvalue


if __name__ == '__main__':
    coriolis_setup()
    kwargs = {}
    success = scriptMain(**kwargs)
    shellSuccess = 0
    if not success:
        shellSuccess = 1

    sys.exit(shellSuccess)
