# Pinouts (PinMux)
auto-generated by [[pinouts.py]]

[[!toc  ]]


## Bank N (32 pins, width 2)

| Pin | Mux0        | Mux1        | Mux2        | Mux3        |
| --- | ----------- | ----------- | ----------- | ----------- |
|   0 | N VSSE_0    |             |
|   1 | N VDDE_0    |             |
|   2 | N SDR_DQM0  |             |
|   3 | N SDR_D0    |             |
|   4 | N SDR_D1    |             |
|   5 | N SDR_D2    |             |
|   6 | N SDR_D3    |             |
|   7 | N SDR_D4    |             |
|   8 | N SDR_D5    |             |
|   9 | N SDR_D6    |             |
|  10 | N SDR_D7    |             |
|  11 | N SDR_AD0   |             |
|  12 | N SDR_AD1   |             |
|  13 | N SDR_AD2   |             |
|  14 | N SDR_AD3   |             |
|  15 | N SDR_AD4   |             |
|  16 | N SDR_AD5   |             |
|  17 | N SDR_AD6   |             |
|  18 | N SDR_AD7   |             |
|  19 | N SDR_AD8   |             |
|  20 | N SDR_AD9   |             |
|  21 | N SDR_BA0   |             |
|  22 | N SDR_BA1   |             |
|  23 | N SDR_CLK   |             |
|  24 | N SDR_CKE   |             |
|  25 | N SDR_RASn  |             |
|  26 | N SDR_CASn  |             |
|  27 | N SDR_WEn   |             |
|  28 | N SDR_CSn0  |             |
|  30 | N VSSI_0    |             |
|  31 | N VDDI_0    |             |

## Bank E (32 pins, width 2)

| Pin | Mux0        | Mux1        | Mux2        | Mux3        |
| --- | ----------- | ----------- | ----------- | ----------- |
|  32 | E VSSE_1    |             |
|  33 | E VDDE_1    |             |
|  34 | E SDR_AD10  |             |
|  35 | E SDR_AD11  |             |
|  36 | E SDR_AD12  |             |
|  37 | E SDR_DQM1  |             |
|  38 | E SDR_D8    |             |
|  39 | E SDR_D9    |             |
|  40 | E SDR_D10   |             |
|  41 | E SDR_D11   |             |
|  42 | E SDR_D12   |             |
|  43 | E SDR_D13   |             |
|  44 | E SDR_D14   |             |
|  45 | E SDR_D15   |             |
|  46 | E VSSI_1    |             |
|  47 | E VDDI_1    |             |
|  48 | E GPIOE_E8  |             |
|  49 | E GPIOE_E9  |             |
|  50 | E GPIOE_E10 |             |
|  51 | E GPIOE_E11 |             |
|  52 | E GPIOE_E12 |             |
|  53 | E GPIOE_E13 |             |
|  54 | E GPIOE_E14 |             |
|  55 | E GPIOE_E15 |             |
|  57 | E JTAG_TMS  |             |
|  58 | E JTAG_TDI  |             |
|  59 | E JTAG_TDO  |             |
|  60 | E JTAG_TCK  |             |

## Bank S (32 pins, width 2)

| Pin | Mux0        | Mux1        | Mux2        | Mux3        |
| --- | ----------- | ----------- | ----------- | ----------- |
|  64 | S VSSI_2    |             |
|  65 | S VDDI_2    |             |
|  66 | S MTWI_SDA  |             |
|  67 | S MTWI_SCL  |             |
|  72 | S MSPI0_CK  |             |
|  73 | S MSPI0_NSS |             |
|  74 | S MSPI0_MOSI |             |
|  75 | S MSPI0_MISO |             |
|  77 | S UART0_TX  |             |
|  78 | S UART0_RX  |             |
|  79 | S GPIOS_S0  |             |
|  80 | S GPIOS_S1  |             |
|  81 | S GPIOS_S2  |             |
|  82 | S GPIOS_S3  |             |
|  83 | S GPIOS_S4  |             |
|  84 | S GPIOS_S5  |             |
|  85 | S GPIOS_S6  |             |
|  86 | S GPIOS_S7  |             |
|  87 | S SYS_CLK   |             |
|  88 | S SYS_RST   |             |
|  89 | S SYS_PLLCLK |             |
|  90 | S SYS_PLLOUT |             |
|  91 | S SYS_CSEL0 |             |
|  92 | S SYS_CSEL1 |             |
|  93 | S SYS_PLLOCK |             |
|  94 | S VSSI_3    |             |
|  95 | S VDDI_3    |             |

## Bank W (32 pins, width 2)

| Pin | Mux0        | Mux1        | Mux2        | Mux3        |
| --- | ----------- | ----------- | ----------- | ----------- |
|  96 | W VSSE_2    |             |
|  97 | W VDDE_2    |             |
|  98 | W PWM_0     |             |
|  99 | W PWM_1     |             |
| 100 | W EINT_0    |             |
| 101 | W EINT_1    |             |
| 102 | W EINT_2    |             |
| 103 | W MSPI1_CK  |             |
| 104 | W MSPI1_NSS |             |
| 105 | W MSPI1_MOSI |             |
| 106 | W MSPI1_MISO |             |
| 107 | W SD0_CMD   |             |
| 108 | W SD0_CLK   |             |
| 109 | W SD0_D0    |             |
| 110 | W SD0_D1    |             |
| 111 | W SD0_D2    |             |
| 112 | W SD0_D3    |             |
| 126 | W VSSI_4    |             |
| 127 | W VDDI_4    |             |

# Pinouts (Fixed function)

# Functions (PinMux)

auto-generated by [[pinouts.py]]

## EINT

External Interrupt

* EINT_0    : W4/0
* EINT_1    : W5/0
* EINT_2    : W6/0

## GPIO

GPIO

* GPIOE_E10 : E18/0
* GPIOE_E11 : E19/0
* GPIOE_E12 : E20/0
* GPIOE_E13 : E21/0
* GPIOE_E14 : E22/0
* GPIOE_E15 : E23/0
* GPIOE_E8  : E16/0
* GPIOE_E9  : E17/0
* GPIOS_S0  : S15/0
* GPIOS_S1  : S16/0
* GPIOS_S2  : S17/0
* GPIOS_S3  : S18/0
* GPIOS_S4  : S19/0
* GPIOS_S5  : S20/0
* GPIOS_S6  : S21/0
* GPIOS_S7  : S22/0

## JTAG

JTAG

* JTAG_TCK  : E28/0
* JTAG_TDI  : E26/0
* JTAG_TDO  : E27/0
* JTAG_TMS  : E25/0

## MSPI0

SPI Master 1 (general)

* MSPI0_CK  : S8/0
* MSPI0_MISO : S11/0
* MSPI0_MOSI : S10/0
* MSPI0_NSS : S9/0

## MSPI1

SPI Master 2 (SDCard)

* MSPI1_CK  : W7/0
* MSPI1_MISO : W10/0
* MSPI1_MOSI : W9/0
* MSPI1_NSS : W8/0

## MTWI

I2C Master 1

* MTWI_SCL  : S3/0
* MTWI_SDA  : S2/0

## PWM

PWM

* PWM_0     : W2/0
* PWM_1     : W3/0

## SD0

SD/MMC 1

* SD0_CLK   : W12/0
* SD0_CMD   : W11/0
* SD0_D0    : W13/0
* SD0_D1    : W14/0
* SD0_D2    : W15/0
* SD0_D3    : W16/0

## SDR

SDRAM

* SDR_AD0   : N11/0
* SDR_AD1   : N12/0
* SDR_AD10  : E2/0
* SDR_AD11  : E3/0
* SDR_AD12  : E4/0
* SDR_AD2   : N13/0
* SDR_AD3   : N14/0
* SDR_AD4   : N15/0
* SDR_AD5   : N16/0
* SDR_AD6   : N17/0
* SDR_AD7   : N18/0
* SDR_AD8   : N19/0
* SDR_AD9   : N20/0
* SDR_BA0   : N21/0
* SDR_BA1   : N22/0
* SDR_CASn  : N26/0
* SDR_CKE   : N24/0
* SDR_CLK   : N23/0
* SDR_CSn0  : N28/0
* SDR_D0    : N3/0
* SDR_D1    : N4/0
* SDR_D10   : E8/0
* SDR_D11   : E9/0
* SDR_D12   : E10/0
* SDR_D13   : E11/0
* SDR_D14   : E12/0
* SDR_D15   : E13/0
* SDR_D2    : N5/0
* SDR_D3    : N6/0
* SDR_D4    : N7/0
* SDR_D5    : N8/0
* SDR_D6    : N9/0
* SDR_D7    : N10/0
* SDR_D8    : E6/0
* SDR_D9    : E7/0
* SDR_DQM0  : N2/0
* SDR_DQM1  : E5/0
* SDR_RASn  : N25/0
* SDR_WEn   : N27/0

## SYS

System Control

* SYS_CLK   : S23/0
* SYS_CSEL0 : S27/0
* SYS_CSEL1 : S28/0
* SYS_PLLCLK : S25/0
* SYS_PLLOCK : S29/0
* SYS_PLLOUT : S26/0
* SYS_RST   : S24/0

## UART0

UART (TX/RX) 1

* UART0_RX  : S14/0
* UART0_TX  : S13/0

## VDD

Power

* VDDE_0    : N1/0
* VDDE_1    : E1/0
* VDDE_2    : W1/0
* VDDI_0    : N31/0
* VDDI_1    : E15/0
* VDDI_2    : S1/0
* VDDI_3    : S31/0
* VDDI_4    : W31/0

## VSS

GND

* VSSE_0    : N0/0
* VSSE_1    : E0/0
* VSSE_2    : W0/0
* VSSI_0    : N30/0
* VSSI_1    : E14/0
* VSSI_2    : S0/0
* VSSI_3    : S30/0
* VSSI_4    : W30/0

# Pinmap for Libre-SOC 180nm

## SD0

user-facing: internal (on Card), multiplexed with JTAG
and UART2, for debug purposes

* SD0_CMD 107 W11/0
* SD0_CLK 108 W12/0
* SD0_D0 109 W13/0
* SD0_D1 110 W14/0
* SD0_D2 111 W15/0
* SD0_D3 112 W16/0

## UART0



* UART0_TX 77 S13/0
* UART0_RX 78 S14/0

## GPIOS

* GPIOS_S0 79 S15/0
* GPIOS_S1 80 S16/0
* GPIOS_S2 81 S17/0
* GPIOS_S3 82 S18/0
* GPIOS_S4 83 S19/0
* GPIOS_S5 84 S20/0
* GPIOS_S6 85 S21/0
* GPIOS_S7 86 S22/0

## GPIOE

* GPIOE_E8 48 E16/0
* GPIOE_E9 49 E17/0
* GPIOE_E10 50 E18/0
* GPIOE_E11 51 E19/0
* GPIOE_E12 52 E20/0
* GPIOE_E13 53 E21/0
* GPIOE_E14 54 E22/0
* GPIOE_E15 55 E23/0

## JTAG

* JTAG_TMS 57 E25/0
* JTAG_TDI 58 E26/0
* JTAG_TDO 59 E27/0
* JTAG_TCK 60 E28/0

## PWM

* PWM_0 98 W2/0
* PWM_1 99 W3/0

## EINT

* EINT_0 100 W4/0
* EINT_1 101 W5/0
* EINT_2 102 W6/0

## VDD

* VDDE_0 1 N1/0
* VDDI_0 31 N31/0
* VDDE_1 33 E1/0

## VSS

* VSSE_0 0 N0/0
* VSSI_0 30 N30/0
* VSSE_1 32 E0/0

## SYS



* SYS_CLK 87 S23/0
* SYS_RST 88 S24/0
* SYS_PLLCLK 89 S25/0
* SYS_PLLOUT 90 S26/0
* SYS_CSEL0 91 S27/0
* SYS_CSEL1 92 S28/0
* SYS_PLLOCK 93 S29/0

## MTWI

I2C.


* MTWI_SDA 66 S2/0
* MTWI_SCL 67 S3/0

## MSPI0

* MSPI0_CK 72 S8/0
* MSPI0_NSS 73 S9/0
* MSPI0_MOSI 74 S10/0
* MSPI0_MISO 75 S11/0

## MSPI1



* MSPI1_CK 103 W7/0
* MSPI1_NSS 104 W8/0
* MSPI1_MOSI 105 W9/0
* MSPI1_MISO 106 W10/0

## SDR



* SDR_DQM0 2 N2/0
* SDR_D0 3 N3/0
* SDR_D1 4 N4/0
* SDR_D2 5 N5/0
* SDR_D3 6 N6/0
* SDR_D4 7 N7/0
* SDR_D5 8 N8/0
* SDR_D6 9 N9/0
* SDR_D7 10 N10/0
* SDR_AD0 11 N11/0
* SDR_AD1 12 N12/0
* SDR_AD2 13 N13/0
* SDR_AD3 14 N14/0
* SDR_AD4 15 N15/0
* SDR_AD5 16 N16/0
* SDR_AD6 17 N17/0
* SDR_AD7 18 N18/0
* SDR_AD8 19 N19/0
* SDR_AD9 20 N20/0
* SDR_BA0 21 N21/0
* SDR_BA1 22 N22/0
* SDR_CLK 23 N23/0
* SDR_CKE 24 N24/0
* SDR_RASn 25 N25/0
* SDR_CASn 26 N26/0
* SDR_WEn 27 N27/0
* SDR_CSn0 28 N28/0
* SDR_AD10 34 E2/0
* SDR_AD11 35 E3/0
* SDR_AD12 36 E4/0
* SDR_DQM1 37 E5/0
* SDR_D8 38 E6/0
* SDR_D9 39 E7/0
* SDR_D10 40 E8/0
* SDR_D11 41 E9/0
* SDR_D12 42 E10/0
* SDR_D13 43 E11/0
* SDR_D14 44 E12/0
* SDR_D15 45 E13/0

## Unused Pinouts (spare as GPIO) for 'Libre-SOC 180nm'

| Pin | Mux0        | Mux1        | Mux2        | Mux3        |
| --- | ----------- | ----------- | ----------- | ----------- |
|  46 | E VSSI_1    |             |             |             |
|  47 | E VDDI_1    |             |             |             |
|  64 | S VSSI_2    |             |             |             |
|  65 | S VDDI_2    |             |             |             |
|  94 | S VSSI_3    |             |             |             |
|  95 | S VDDI_3    |             |             |             |
|  96 | W VSSE_2    |             |             |             |
|  97 | W VDDE_2    |             |             |             |
| 126 | W VSSI_4    |             |             |             |
| 127 | W VDDI_4    |             |             |             |

# Reference Datasheets

datasheets and pinout links

* <http://datasheets.chipdb.org/AMD/8018x/80186/amd-80186.pdf>
* <http://hands.com/~lkcl/eoma/shenzen/frida/FRD144A2701.pdf>
* <http://pinouts.ru/Memory/sdcard_pinout.shtml>
* p8 <http://www.onfi.org/~/media/onfi/specs/onfi_2_0_gold.pdf?la=en>
* <https://www.heyrick.co.uk/blog/files/datasheets/dm9000aep.pdf>
* <http://cache.freescale.com/files/microcontrollers/doc/app_note/AN4393.pdf>
* <https://www.nxp.com/docs/en/data-sheet/MCF54418.pdf>
* ULPI OTG PHY, ST <http://www.st.com/en/interfaces-and-transceivers/stulpi01a.html>
* ULPI OTG PHY, TI TUSB1210 <http://ti.com/product/TUSB1210/>

# Pin Bank starting points and lengths

* E 32 32 2
* N 0 32 2
* S 64 32 2
* W 96 32 2
