#!/usr/bin/env python

from helpers import l, u, n
import os
import json

def _byteify(data, ignore_dicts = False):
    # if this is a unicode string, return its string representation
    if isinstance(data, unicode):
        return data.encode('utf-8')
    # if this is a list of values, return list of byteified values
    if isinstance(data, list):
        return [ _byteify(item, ignore_dicts=True) for item in data ]
    # if this is a dictionary, return dictionary of byteified keys and values
    # but only if we haven't already byteified it
    if isinstance(data, dict) and not ignore_dicts:
        return dict((_byteify(key, ignore_dicts=True),
                    _byteify(value, ignore_dicts=True))
                        for key, value in data.iteritems())
    # if it's anything else, return it in its original form
    return data

# load JSON-formatted pad info from pinmux
pth = os.path.abspath(__file__)
pth = os.path.split(pth)[0]
print "path", pth
with open("%s/ls180/litex_pinpads.json" % pth) as f:
    txt = f.read()
chip = json.loads(txt, object_hook=_byteify)
chip = _byteify(chip, ignore_dicts=True)
print chip

chip.update({ 'pads.ioPadGauge' : 'pxlib',
          # core option (big, time-consuming)
          #'core.size'       : ( l(28000), l(28000) ),
          #'chip.size'       : ( l(30200), l(30200) ),
          # no-core option (test_issuer but no actual core)
          'core.size'       : ( l(13000), l(13000) ),
          'chip.size'       : ( l(14400), l(14400) ),
          'pads.useCoreSize': True,
          'chip.clockTree'  : True,
       })
