(* \nmigen.hierarchy  = "test_issuer.ti.sram4k_0.spblock512w64b8w_0" *)
(* generator = "nMigen" *)
(* blackbox = 1 *)
module spblock512w64b8w_0(a, d, q, we, clk);
	input [8:0] a;
	input [63:0] d;
	output [63:0] q;
	input [7:0] we;
 	input clk;
endmodule // SPBlock_512W64B8W

