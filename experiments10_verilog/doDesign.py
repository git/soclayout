
from   __future__ import print_function
import sys
import traceback
import CRL
import helpers
from   helpers.io import ErrorMessage
from   helpers.io import WarningMessage
from   helpers    import trace
from   helpers    import l, u, n
import plugins
from   Hurricane  import DbU
from   plugins.alpha.block.block         import Block
from   plugins.alpha.block.configuration import IoPin
from   plugins.alpha.block.configuration import GaugeConf
from   plugins.alpha.core2chip.niolib    import CoreToChip
#from   plugins.alpha.core2chip.libresocio    import CoreToChip
from   plugins.alpha.chip.configuration  import ChipConf
from   plugins.alpha.chip.chip           import Chip


af = CRL.AllianceFramework.get()


def scriptMain ( **kw ):
    """The mandatory function to be called by Coriolis CGT/Unicorn."""
    global af
    rvalue = True
    try:
        coreSize = 10000
        helpers.setTraceLevel( 550 )
        cell, editor = plugins.kwParseMain( **kw )
        cell = af.getCell( 'add', CRL.Catalog.State.Logical )
        if cell is None:
            print( ErrorMessage( 2, 'doDesign.scriptMain(): Unable to load cell "{}".'.format('adder') ))
            sys.exit( 1 )
        if editor: editor.setCell( cell ) 
        #     Spec: 
        # | Side        | Pos | Instance     | Pad net   |Core net | Direction |
        ioPadsSpec = [
           (IoPin.SOUTH, None, 'p_a0'       , 'a(0)'   , 'a(0)'     )
         , (IoPin.SOUTH, None, 'p_a1'       , 'a(1)'   , 'a(1)'     )
         , (IoPin.SOUTH, None, 'iopower_0'  , 'iovdd'  )
         , (IoPin.SOUTH, None, 'power_0'    , 'vdd'    )
         , (IoPin.SOUTH, None, 'p_a2'       , 'a(2)'   , 'a(2)'     )
         , (IoPin.SOUTH, None, 'p_b3'       , 'b(3)'   , 'b(3)'     )
         , (IoPin.SOUTH, None, 'p_pll_vco'    , 'pll_vco' , 'pll_vco'     )
         , (IoPin.EAST , None, 'p_jtag_tms' , 'jtag_tms'    , 'jtag_tms'      )
         , (IoPin.EAST , None, 'p_jtag_tdo' , 'jtag_tdo'    , 'jtag_tdo'      )
         , (IoPin.EAST , None, 'ground_0'   , 'vss'    )
         , (IoPin.EAST , None, 'p_sys_clk'  , 'clk'    , 'clk'      )
         , (IoPin.EAST , None, 'p_jtag_tck' , 'jtag_tck'    , 'jtag_tck'      )
         , (IoPin.EAST , None, 'p_jtag_tdi' , 'jtag_tdi'    , 'jtag_tdi'      )
         , (IoPin.EAST , None, 'p_b2'       , 'b(2)'   , 'b(2)'     )
         , (IoPin.EAST , None, 'p_b0'       , 'b(0)'   , 'b(0)'     )
         , (IoPin.NORTH, None, 'ioground_0' , 'iovss'  )
         , (IoPin.NORTH, None, 'p_b1'       , 'b(1)'   , 'b(1)'     )
         , (IoPin.NORTH, None, 'ground_1'   , 'vss'    )
         , (IoPin.NORTH, None, 'p_pll_test'    , 'pll_test' , 'pll_test'     )
         , (IoPin.NORTH, None, 'p_pll_a0'    , 'a0' , 'a0'     )
         , (IoPin.NORTH, None, 'p_pll_a1'    , 'a1' , 'a1'     )
         , (IoPin.NORTH, None, 'p_sys_rst'  , 'rst'    , 'rst'      )
         , (IoPin.WEST , None, 'p_f3'       , 'f(3)'   , 'f(3)'     )
         , (IoPin.WEST , None, 'p_f2'       , 'f(2)'   , 'f(2)'     )
         , (IoPin.WEST , None, 'power_1'    , 'vdd'    )
         , (IoPin.WEST , None, 'p_coresync_clk', 'coresync_clk', 'coresync_clk' )
         #, (IoPin.WEST , None, 'coresync_rst', 'coresync_rst', 'coresync_rst' )
         , (IoPin.WEST , None, 'p_f1'       , 'f(1)'   , 'f(1)'     )
         , (IoPin.WEST , None, 'p_f0'       , 'f(0)'   , 'f(0)'     )
         , (IoPin.WEST , None, 'p_a3'       , 'a(3)'   , 'a(3)'     )
        ]
        adderConf = ChipConf( cell, ioPads=ioPadsSpec )
        adderConf.cfg.etesian.bloat = 'nsxlib'
        adderConf.cfg.etesian.uniformDensity = True
        adderConf.cfg.etesian.aspectRatio = 1.0
        adderConf.cfg.etesian.spaceMargin = 0.05
        adderConf.cfg.block.spareSide = l(700)
        adderConf.cfg.chip.padCoreSide = 'North'
        adderConf.editor = editor
        adderConf.useSpares = True
        adderConf.useClockTree = True
        #adderConf.useHFNS = True
        adderConf.cfg.katana.hTracksReservedMin = 9
        adderConf.cfg.katana.vTracksReservedMin = 2
        adderConf.bColumns = 2
        adderConf.bRows = 2
        adderConf.chipConf.name = 'chip'
        #adderConf.chipConf.ioPadGauge = 'LibreSOCIO'
        adderConf.chipConf.ioPadGauge = 'niolib'
        adderConf.useHTree('coresync_clk')
        adderConf.useHTree('jtag_tck_from_pad')
        adderConf.useHTree('clk_from_pad')
        adderConf.coreSize = ( l(coreSize), l(coreSize) )
        adderConf.chipSize = ( l(coreSize+3500), l(coreSize+3500) )
        adderToChip = CoreToChip( adderConf )
        adderToChip.buildChip()

        chipBuilder = Chip( adderConf )
        chipBuilder.doChipFloorplan()

        rvalue = chipBuilder.doPnR()
        chipBuilder.save()
        CRL.Gds.save(adderConf.chip)

    except Exception, e:
        helpers.io.catch( e )
        rvalue = False
    sys.stdout.flush()
    sys.stderr.flush()
    return rvalue
