
===============================
Status Report on Block Building
===============================


April 04, 2020 -- Optimized Placement
=====================================

The placement of the ALU16 as been optimized.

* The gates of the top level netlist that form a regular array (datapath)
  have been manually placed between the add & sub blocks.

* The connectors of the add & sub blocks have been put face to face to have
  as much as possible straight lines.

* The connector of the add & sub block have been placed in a "slice" 
  way: that is a(x), b(x) & o(x) all in the same slice. A slice is one
  horizontal row of standard cells.

As a comparison, an alternate version of the script "doAlu16Flat.py" has
been created to make an area comparison if everything is left to the
automaic P&R. For such a small example, they clearly win. Have to confirm
that on bigger ones.

.. note:: **Never rotate a block**. The routing directions are defined
	  for all the chip area. And the connectors will be in the
	  wrong layers, generating impossible routing configurations.
	    This may be possible, but at the cost of *wrapping* the
	  block in a small corona (has yet to be devellopped).


Open/Remaining Questions
~~~~~~~~~~~~~~~~~~~~~~~~

* The placement is manual, but the routing is automatic for the center
  part. The router still makes some "bad choices", so it would be
  interesting to do a manual routing too.

* The layout is not completely finished: the filler cells are missing
  at top level. Have to separate and export that lone feature from
  Etesian (placer).

* The way to "rebuild" the datapath information is not robust. It relies
  on the way Yosys/ABC synthesis the mux+DFF, if it changes, it will fail.

* Corollary: it may make senses to directly describe some part of the
  design at cell level and not let it synthesis by Yosys, but in this
  case how to "import" at nMigen level.

* The place_datapath() and match_instance() method may be moved down to
  the Module class.

* Policy about how soon to convert into DbU.

* Specify the semantic of Module. Is it an instance or a model, can they
  be re-used in multiple blocks (should they?).
