#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import print_function
import sys
import re

import CRL
import Cfg
from Hurricane import Box
from Hurricane import Transformation
from Hurricane import Breakpoint
from Hurricane import Instance
from coriolis2.settings import af
from utils import Module, SessionManager, Config

import symbolic.cmos  # do not remove

BIT_WIDTH = 16
widths = {16 : 465.0, 32: 800.0}
offsets = {16 : 0.0, 32: 200.0}

def coriolis_setup():
    with Config(Cfg.Parameter.Priority.UserFile) as cfg:
        cfg.misc_catchCore = False
        cfg.misc_info = False
        cfg.misc_paranoid = False
        cfg.misc_bug = False
        cfg.misc_logMode = True
        cfg.misc_verboseLevel1 = True
        cfg.misc_verboseLevel2 = True
        cfg.etesian_effort = 2
        cfg.etesian_spaceMargin = "5.0%"
        cfg.etesian_aspectRatio = "100.0%"
        cfg.etesian_uniformDensity = True
        cfg.anabatic_edgeLenght = 24
        cfg.anabatic_edgeWidth = 8
        cfg.anabatic_topRoutingLayer = 'METAL5'
        cfg.katana_searchHalo = 30
        cfg.katana_eventsLimit = 1000000
        cfg.katana_hTracksReservedLocal = 7
        cfg.katana_vTracksReservedLocal = 6

        env = af.getEnvironment()
        env.setCLOCK('^clk$|m_clock')
        env.setPOWER('vdd')
        env.setGROUND('vss')


class AddSub(Module):

    def build(self):
        """ Main routine. """

        return True


class ALU16(Module):

    def save(self):
        self.name = self.name + '_r'
        self.af.saveCell(self.cell, CRL.Catalog.State.Views)
        super(ALU16, self).save()

    def build(self):

        h_margin = 0.0
        v_margin = 50.0

        if not self.build_submodules():
            return False

        # at this point we have the (auto-calculated) submodules' dimensions
        # in their `ab` properties.

        add, sub = self.submodules

        with SessionManager():
            # TODO: replace with some form of lazy evaluation?
            y_north = self.from_dbu(self.ab.getYMax())
            for pin_conf in self.north_pins:
                pin_conf['y'] = y_north

            self.create_pins()

        if self.editor:
            self.editor.setCell(self.cell)

        self.place() # place only
        Breakpoint.stop(1, 'After ALU16 placement.')
        result = self.route()

        self.save()
        return result


def scriptMain(editor=None, **kwargs):
    coriolis_setup()

    add = AddSub(
        'add', editor,
        pads={
            'b({})'.format(BIT_WIDTH-1): (
                'BLOCKAGE2', 'BLOCKAGE3', 'BLOCKAGE4',
            ),
        },
        orientation=Transformation.Orientation.ID,
    )
    sub = AddSub(
        'sub', editor,
        pads={
            'b({})'.format(BIT_WIDTH-1): (
                'BLOCKAGE2', 'BLOCKAGE3', 'BLOCKAGE4',
            ),
        },
        orientation=Transformation.Orientation.ID,
    )

    o = offsets[BIT_WIDTH]
    alu16 = ALU16(
        'alu16', editor, submodules=[add, sub],
        north_pins=[
            {'net': 'o({})', 'x': o+365.0, 'delta': -5.0, 'repeat': BIT_WIDTH},
            {'net': 'op'},
        ],
        south_pins=[
            {'net': 'a({})', 'x': o+205.0, 'delta': 5.0, 'repeat': BIT_WIDTH},
            {'net': 'b({})', 'x': o+295.0, 'delta': 5.0, 'repeat': BIT_WIDTH},
        ],
        west_pins=[
            {'net': 'rst', 'y': 10.0, 'layer': 'METAL2'},
        ],
    )
    alu16.set_ab( widths[BIT_WIDTH], BIT_WIDTH * 50.0 )
    return alu16.build()


if __name__ == '__main__':
    kwargs = {}
    success = scriptMain(**kwargs)
    shellSuccess = 0
    if not success:
        shellSuccess = 1

    sys.exit(shellSuccess)
