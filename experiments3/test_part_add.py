#!/usr/bin/env python3
# SPDX-License-Identifier: LGPL-2.1-or-later
# See Notices.txt for copyright information

from nmigen import Signal, Module, Elaboratable
from nmigen.cli import rtlil

from ieee754.part.partsig import PartitionedSignal

def create_ilang(dut, traces, test_name):
    vl = rtlil.convert(dut, ports=traces, name=test_name)
    with open("%s.il" % test_name, "w") as f:
        f.write(vl)



class TestAddMod(Elaboratable):
    def __init__(self, width, partpoints):
        self.partpoints = partpoints
        self.a = PartitionedSignal(partpoints, width)
        self.b = PartitionedSignal(partpoints, width)
        self.add_output = Signal(width)
        self.carry_in = Signal(len(partpoints)+1)
        self.add_carry_out = Signal(len(partpoints)+1)

    def elaborate(self, platform):
        m = Module()
        comb = m.d.comb
        sync = m.d.sync
        self.a.set_module(m)
        self.b.set_module(m)
        # add
        add_out, add_carry = self.a.add_op(self.a, self.b,
                                           self.carry_in)
        sync += self.add_output.eq(add_out)
        sync += self.add_carry_out.eq(add_carry)

        return m

if __name__ == '__main__':
    width = 16
    pmask = Signal(3)  # divide into 4-bits
    module = TestAddMod(width, pmask)

    create_ilang(module,
                           [pmask,
                            module.a.sig,
                            module.b.sig,
                            module.add_output,
                            module.carry_in,
                            module.add_carry_out,
                           ],
                           "test_part_add")
    print (dir(module))
    add_1 = module.a.m.submodules.add_1
    print (dir(add_1.part_pts))
    create_ilang(add_1,
                           [pmask,
                            add_1.a,
                            add_1.b,
                            add_1.output,
                            add_1.carry_in,
                            add_1.carry_out,
                           ],
                           "test_add")
