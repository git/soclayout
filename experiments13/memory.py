from nmigen import Elaboratable, Cat, Module, Signal, Instance, Memory
from nmigen.cli import rtlil


class ADD(Elaboratable):
    def __init__(self, width):
        self.a   = Signal(width)
        self.b   = Signal(width)
        self.f   = Signal(width)
        self.mem   = Memory(width=64, depth=64, init=range(64))

    def elaborate(self, platform):
        m = Module()
        result = Signal.like(self.f)
        m.d.sync += result.eq(self.a + self.b)

        # 64k SRAM instance
        a = Signal(16)
        q = Signal(64) # output
        d = Signal(64) # input
        we = Signal(1)
        # Memory
        m.submodules.rdport = rdport = self.mem.read_port()
        m.submodules.wrport = wrport = self.mem.write_port()
        m.d.comb += [
            rdport.addr.eq(a),
            q.eq(rdport.data),
            wrport.addr.eq(a),
            wrport.data.eq(d),
            wrport.en.eq(we),
        ]

        # connect up some arbitrary signals, to get the memory to be
        # at least mostly instantiated.  
        m.d.comb += a.eq(Cat(self.a, self.b, self.a, self.b)) # 16-bit
        m.d.comb += we.eq(self.a[0] ^ self.b[0])
        # in/out is 4 bit, mem is 64.  just... wrap/add
        for i in range(0,64,4):
            m.d.comb += d[i:i+4].eq(result)
        for i in range(0,64,4):
            result = result + q[i:i+4]
        m.d.comb += self.f.eq(result)

        return m


def create_ilang(dut, ports, test_name):
    vl = rtlil.convert(dut, name=test_name, ports=ports)
    with open("%s.il" % test_name, "w") as f:
        f.write(vl)

if __name__ == "__main__":
    alu = ADD(width=4)
    create_ilang(alu, [alu.a, alu.b, alu.f], "memory")
