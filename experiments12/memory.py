from nmigen import Elaboratable, Cat, Module, Signal, ClockSignal, Instance
from nmigen.cli import rtlil


class ADD(Elaboratable):
    def __init__(self, width):
        self.we  = Signal(8)
        self.a   = Signal(width)
        self.b   = Signal(width)
        self.f   = Signal(width)

    def elaborate(self, platform):
        m = Module()
        result = Signal.like(self.f)
        m.d.sync += result.eq(self.a + self.b)

        # 64k SRAM instance
        a = Signal(9)
        q = Signal(64) # output
        d = Signal(64) # input
        sram = Instance("spblock_512w64b8w", i_a=a, o_q=q, i_d=d,
                                             i_we=self.we, i_clk=ClockSignal())
        m.submodules += sram

        # connect up some arbitrary signals
        m.d.comb += a.eq(Cat(self.a, self.b, self.a[0]))
        m.d.comb += d.eq(result)
        m.d.comb += self.f.eq(q)

        return m


def create_ilang(dut, ports, test_name):
    vl = rtlil.convert(dut, name=test_name, ports=ports)
    with open("%s.il" % test_name, "w") as f:
        f.write(vl)

if __name__ == "__main__":
    alu = ADD(width=64)
    create_ilang(alu, [alu.a, alu.b, alu.f, alu.we], "memory")
